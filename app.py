import tkinter as tk
from tkinter import *
from tkhtmlview import HTMLLabel
from markdown2 import Markdown

class MarkdownDisplay(HTMLLabel):
    def __init__(self, master=None):
        HTMLLabel.__init__(self, master=master, width='1')
        
    def set_content(self, md: str) -> None:
        md2html = Markdown()
        html = md2html.convert(md)
        self.set_html(html)


class SideBySide(Frame):
    def __init__(self, master=None) -> None:
        Frame.__init__(self, master)
        self.pack(fill=BOTH, expand=1)
        
        self.left = Text(self, width='1')
        self.left.pack(fill=BOTH, expand=1, side=LEFT)
        self.left.bind('<<Modified>>', self.on_input_change)

        self.right = MarkdownDisplay(self)
        self.right.pack(fill=BOTH, expand=1, side=RIGHT)
        self.right.fit_height()


    def on_input_change(self, event) -> None:
        self.left.edit_modified(0)
        self.right.set_content(self.left.get('1.0', END))



root = tk.Tk()
html_label = SideBySide(root)
html_label.pack()
root.mainloop()
